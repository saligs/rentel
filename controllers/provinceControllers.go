package controllers

import (
	"fmt"
	"encoding/json"
	"go-rentel/models"
	u "go-rentel/utils"
	"net/http"
	"strconv"
	"github.com/gorilla/mux"
)

var CreateProvince = func(w http.ResponseWriter, r *http.Request) {

	province := &models.Province{}
	err := json.NewDecoder(r.Body).Decode(province)
	if err != nil {
		u.Respond(w, u.Message(false, "Error while decoding request body"))
		return
	}

	resp := province.Create()
	u.Respond(w, resp)
}

var FetchProvince = func(w http.ResponseWriter, r *http.Request) {

	data := models.FetchProvince();
	resp := u.Message(true, "success")
	resp["data"] = data
	u.Respond(w, resp)
}

var GetProvince = func(w http.ResponseWriter, r *http.Request) {

	resp := u.Message(true, "success")
	params := mux.Vars(r)
	id, err := strconv.Atoi(params["id"])
	if err != nil {
		fmt.Println(err)
		//The passed path parameter is not an integer
		u.Respond(w, u.Message(false, "There was an error in your request"))
		return
	}
	
	data := models.GetProvince(uint(id))
	resp["data"] = data
	u.Respond(w, resp)
}

var UpdateProvince = func(w http.ResponseWriter, r *http.Request) {
	
	resp := u.Message(true, "success")
	params := mux.Vars(r)
	id, err := strconv.Atoi(params["id"])
	if err != nil {
		fmt.Println(err)
		//The passed path parameter is not an integer
		u.Respond(w, u.Message(false, "There was an error in your request"))
		return
	}

	province := &models.Province{}
	err = json.NewDecoder(r.Body).Decode(province)
	if err != nil {
		u.Respond(w, u.Message(false, "Error while decoding request body"))
		return
	}

	data := models.UpdateProvince(uint(id), province)
	resp["data"] = data
	u.Respond(w, resp)
}

var DeleteProvince = func(w http.ResponseWriter, r *http.Request) {

	params := mux.Vars(r)
	id, err := strconv.Atoi(params["id"])
	if err != nil {
		fmt.Println(err)
		//The passed path parameter is not an integer
		u.Respond(w, u.Message(false, "There was an error in your request"))
		return
	}

	resp := models.DeleteProvince(uint(id))
	u.Respond(w, resp)
}