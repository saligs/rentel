package controllers

import (
	"fmt"
	"encoding/json"
	"go-rentel/models"
	u "go-rentel/utils"
	"net/http"
	"strconv"
	"github.com/gorilla/mux"
)

var CreateCity = func(w http.ResponseWriter, r *http.Request) {

	city := &models.City{}
	err := json.NewDecoder(r.Body).Decode(city)
	if err != nil {
		u.Respond(w, u.Message(false, "Error while decoding request body"))
		return
	}

	resp := city.Create()
	u.Respond(w, resp)
}

var FetchCity = func(w http.ResponseWriter, r *http.Request) {

	data := models.FetchCity();
	resp := u.Message(true, "success")
	resp["data"] = data
	u.Respond(w, resp)
}

var FetchCityByProvince = func(w http.ResponseWriter, r *http.Request) {

	params := mux.Vars(r)
	provinceId, err := strconv.Atoi(params["province_id"])
	if err != nil {
		fmt.Println(err)
		//The passed path parameter is not an integer
		u.Respond(w, u.Message(false, "There was an error in your request"))
		return
	}
	data := models.FetchCityByProvince(provinceId);
	resp := u.Message(true, "success")
	resp["data"] = data
	u.Respond(w, resp)
}

var GetCity = func(w http.ResponseWriter, r *http.Request) {

	resp := u.Message(true, "success")
	params := mux.Vars(r)
	id, err := strconv.Atoi(params["id"])
	if err != nil {
		fmt.Println(err)
		//The passed path parameter is not an integer
		u.Respond(w, u.Message(false, "There was an error in your request"))
		return
	}
	
	data := models.GetCity(uint(id))
	resp["data"] = data
	u.Respond(w, resp)
}

var UpdateCity = func(w http.ResponseWriter, r *http.Request) {
	
	resp := u.Message(true, "success")
	params := mux.Vars(r)
	id, err := strconv.Atoi(params["id"])
	if err != nil {
		fmt.Println(err)
		//The passed path parameter is not an integer
		u.Respond(w, u.Message(false, "There was an error in your request"))
		return
	}

	city := &models.City{}
	err = json.NewDecoder(r.Body).Decode(city)
	if err != nil {
		u.Respond(w, u.Message(false, "Error while decoding request body"))
		return
	}

	data := models.UpdateCity(uint(id), city)
	resp["data"] = data
	u.Respond(w, resp)
}

var DeleteCity = func(w http.ResponseWriter, r *http.Request) {

	params := mux.Vars(r)
	id, err := strconv.Atoi(params["id"])
	if err != nil {
		fmt.Println(err)
		//The passed path parameter is not an integer
		u.Respond(w, u.Message(false, "There was an error in your request"))
		return
	}

	resp := models.DeleteCity(uint(id))
	u.Respond(w, resp)
}