package controllers

import (
	"fmt"
	"encoding/json"
	"go-rentel/models"
	u "go-rentel/utils"
	"net/http"
	"strconv"
	"github.com/gorilla/mux"
)

var CreateCar = func(w http.ResponseWriter, r *http.Request) {

	user := r.Context().Value("user").(uint)
	car := &models.Car{}
	err := json.NewDecoder(r.Body).Decode(car)
	if err != nil {
		u.Respond(w, u.Message(false, "Error while decoding request body"))
		return
	}

	car.UserId = user;
	resp := car.Create()
	u.Respond(w, resp)
}

var FetchCar = func(w http.ResponseWriter, r *http.Request) {

	data := models.FetchCar();
	resp := u.Message(true, "success")
	resp["data"] = data
	u.Respond(w, resp)
}

var GetCar = func(w http.ResponseWriter, r *http.Request) {

	resp := u.Message(true, "success")
	params := mux.Vars(r)
	id, err := strconv.Atoi(params["id"])
	if err != nil {
		fmt.Println(err)
		//The passed path parameter is not an integer
		u.Respond(w, u.Message(false, "There was an error in your request"))
		return
	}
	
	data := models.GetCar(uint(id))
	resp["data"] = data
	u.Respond(w, resp)
}

var UpdateCar = func(w http.ResponseWriter, r *http.Request) {
	
	user := r.Context().Value("user").(uint)
	resp := u.Message(true, "success")
	params := mux.Vars(r)
	id, err := strconv.Atoi(params["id"])
	if err != nil {
		fmt.Println(err)
		//The passed path parameter is not an integer
		u.Respond(w, u.Message(false, "There was an error in your request"))
		return
	}

	car := &models.Car{}
	err = json.NewDecoder(r.Body).Decode(car)
	if err != nil {
		u.Respond(w, u.Message(false, "Error while decoding request body"))
		return
	}

	data := models.UpdateCar(uint(id), user, car)
	resp["data"] = data
	u.Respond(w, resp)
}

var DeleteCar = func(w http.ResponseWriter, r *http.Request) {

	user := r.Context().Value("user").(uint)
	params := mux.Vars(r)
	id, err := strconv.Atoi(params["id"])
	if err != nil {
		fmt.Println(err)
		//The passed path parameter is not an integer
		u.Respond(w, u.Message(false, "There was an error in your request"))
		return
	}

	resp := models.DeleteCar(uint(id), user)
	u.Respond(w, resp)
}