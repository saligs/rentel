package controllers

import (
	"fmt"
	"encoding/json"
	"go-rentel/models"
	u "go-rentel/utils"
	"net/http"
	"strconv"
	"github.com/gorilla/mux"
)

var CreateCarType = func(w http.ResponseWriter, r *http.Request) {

	carType := &models.CarType{}
	err := json.NewDecoder(r.Body).Decode(carType)
	if err != nil {
		u.Respond(w, u.Message(false, "Error while decoding request body"))
		return
	}

	resp := carType.Create()
	u.Respond(w, resp)
}

var FetchCarType = func(w http.ResponseWriter, r *http.Request) {

	data := models.FetchCarType();
	resp := u.Message(true, "success")
	resp["data"] = data
	u.Respond(w, resp)
}

var GetCarType = func(w http.ResponseWriter, r *http.Request) {

	resp := u.Message(true, "success")
	params := mux.Vars(r)
	id, err := strconv.Atoi(params["id"])
	if err != nil {
		fmt.Println(err)
		//The passed path parameter is not an integer
		u.Respond(w, u.Message(false, "There was an error in your request"))
		return
	}
	
	data := models.GetCarType(uint(id))
	resp["data"] = data
	u.Respond(w, resp)
}

var UpdateCarType = func(w http.ResponseWriter, r *http.Request) {
	
	resp := u.Message(true, "success")
	params := mux.Vars(r)
	id, err := strconv.Atoi(params["id"])
	if err != nil {
		fmt.Println(err)
		//The passed path parameter is not an integer
		u.Respond(w, u.Message(false, "There was an error in your request"))
		return
	}

	carType := &models.CarType{}
	err = json.NewDecoder(r.Body).Decode(carType)
	if err != nil {
		u.Respond(w, u.Message(false, "Error while decoding request body"))
		return
	}

	data := models.UpdateCarType(uint(id), carType)
	resp["data"] = data
	u.Respond(w, resp)
}

var DeleteCarType = func(w http.ResponseWriter, r *http.Request) {

	params := mux.Vars(r)
	id, err := strconv.Atoi(params["id"])
	if err != nil {
		fmt.Println(err)
		//The passed path parameter is not an integer
		u.Respond(w, u.Message(false, "There was an error in your request"))
		return
	}

	resp := models.DeleteCarType(uint(id))
	u.Respond(w, resp)
}