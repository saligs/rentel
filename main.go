package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"go-rentel/app"
	"go-rentel/controllers"
	"net/http"
	"os"
)

func main() {

	router := mux.NewRouter()

	router.HandleFunc("/api/user/new", controllers.CreateAccount).Methods("POST")
	router.HandleFunc("/api/user/login", controllers.Authenticate).Methods("POST")

	router.HandleFunc("/api/cities", controllers.FetchCity).Methods("GET")
	router.HandleFunc("/api/cities/fetch/{province_id}", controllers.FetchCityByProvince).Methods("GET")
	router.HandleFunc("/api/cities/new", controllers.CreateCity).Methods("POST")
	router.HandleFunc("/api/cities/{id}", controllers.GetCity).Methods("GET")
	router.HandleFunc("/api/cities/{id}/edit", controllers.UpdateCity).Methods("POST")
	router.HandleFunc("/api/cities/{id}/destroy", controllers.DeleteCity).Methods("POST")

	router.HandleFunc("/api/provinces", controllers.FetchProvince).Methods("GET")
	router.HandleFunc("/api/provinces/new", controllers.CreateProvince).Methods("POST")
	router.HandleFunc("/api/provinces/{id}", controllers.GetProvince).Methods("GET")
	router.HandleFunc("/api/provinces/{id}/edit", controllers.UpdateProvince).Methods("POST")
	router.HandleFunc("/api/provinces/{id}/destroy", controllers.DeleteProvince).Methods("POST")

	router.HandleFunc("/api/car-types", controllers.FetchCarType).Methods("GET")
	router.HandleFunc("/api/car-types/new", controllers.CreateCarType).Methods("POST")
	router.HandleFunc("/api/car-types/{id}", controllers.GetCarType).Methods("GET")
	router.HandleFunc("/api/car-types/{id}/edit", controllers.UpdateCarType).Methods("POST")
	router.HandleFunc("/api/car-types/{id}/destroy", controllers.DeleteCarType).Methods("POST")

	router.HandleFunc("/api/car", controllers.FetchCar).Methods("GET")
	router.HandleFunc("/api/car/new", controllers.CreateCar).Methods("POST")
	router.HandleFunc("/api/car/{id}", controllers.GetCar).Methods("GET")
	router.HandleFunc("/api/car/{id}/edit", controllers.UpdateCar).Methods("POST")
	router.HandleFunc("/api/car/{id}/destroy", controllers.DeleteCar).Methods("POST")

	router.Use(app.JwtAuthentication) //attach JWT auth middleware

	//router.NotFoundHandler = app.NotFoundHandler

	port := os.Getenv("PORT")
	if port == "" {
		port = "8000" //localhost
	}

	fmt.Println(port)

	err := http.ListenAndServe(":"+port, router) //Launch the app, visit localhost:8000/api
	if err != nil {
		fmt.Print(err)
	}
}
