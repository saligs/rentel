package models

import (
	"github.com/jinzhu/gorm"
	u "go-rentel/utils"
)

type CarPackage struct {
	gorm.Model
	Hour   uint `json:"hour"`
	Price  uint `json:"price"`
	CarId  uint `json:"car_id"`
}

func (carPackage *CarPackage) Validate() (map[string]interface{}, bool) {

	if carPackage.Hour <= 0 {
		return u.Message(false, "Hour should be on the payload"), false
	}

	if carPackage.Price <= 0 {
		return u.Message(false, "Price should be on the payload"), false
	}

	if carPackage.CarId <= 0 {
		return u.Message(false, "Car is not recognized"), false
	}

	//All the required parameters are present
	return u.Message(true, "success"), true
}

func (carPackage *CarPackage) Create() (map[string]interface{}) {

	if resp, ok := carPackage.Validate(); !ok {
		return resp
	}

	GetDB().Create(carPackage)

	resp := u.Message(true, "success")
	resp["carPackage"] = carPackage
	return resp
}

func GetCarPackage(id uint) (*CarPackage) {

	carPackage := &CarPackage{}
	err := GetDB().Table("car_packages").Where("id = ?", id).First(carPackage).Error
	if err != nil {
		return nil
	}
	return carPackage
}

func FetchCarPackage() ([]*CarPackage) {

	car_packages := make([]*CarPackage, 0)
	err := GetDB().Table("car_packages").Find(&car_packages).Error
	if err != nil {
		return nil
	}

	return car_packages
}

func UpdateCarPackage(id uint, newCarPackage *CarPackage) (*CarPackage) {

	err := GetDB().Table("car_packages").Where("id = ?", id).Update(newCarPackage).Error
	if err != nil {
		return nil
	}

	carPackage := &CarPackage{}
	err = GetDB().Table("car_packages").Where("id = ?", id).First(carPackage).Error
	if err != nil {
		return nil
	}
	return carPackage
}

func DeleteCarPackage(id uint) (map[string]interface{}) {
	carPackage := &CarPackage{}
	err := GetDB().Table("car_packages").Where("id = ?", id).First(carPackage).Error
	if err != nil {
		return nil
	}

	GetDB().Delete(&carPackage)

	resp := u.Message(true, "success")
	return resp
}