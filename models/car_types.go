package models

import (
	"github.com/jinzhu/gorm"
	u "go-rentel/utils"
)

type CarType struct {
	gorm.Model
	Name   string `json:"name"`
	Photo  string `json:"photo"`
}

func (carType *CarType) Validate() (map[string]interface{}, bool) {

	if carType.Name == "" {
		return u.Message(false, "Car Type name should be on the payload"), false
	}

	//All the required parameters are present
	return u.Message(true, "success"), true
}

func (carType *CarType) Create() (map[string]interface{}) {

	if resp, ok := carType.Validate(); !ok {
		return resp
	}

	GetDB().Create(carType)

	resp := u.Message(true, "success")
	resp["carType"] = carType
	return resp
}

func GetCarType(id uint) (*CarType) {

	carType := &CarType{}
	err := GetDB().Table("car_types").Where("id = ?", id).First(carType).Error
	if err != nil {
		return nil
	}
	return carType
}

func FetchCarType() ([]*CarType) {

	carTypes := make([]*CarType, 0)
	err := GetDB().Table("car_types").Find(&carTypes).Error
	if err != nil {
		return nil
	}

	return carTypes
}

func UpdateCarType(id uint, newCarType *CarType) (*CarType) {

	err := GetDB().Table("car_types").Where("id = ?", id).Update(newCarType).Error
	if err != nil {
		return nil
	}

	carType := &CarType{}
	err = GetDB().Table("car_types").Where("id = ?", id).First(carType).Error
	if err != nil {
		return nil
	}
	return carType
}

func DeleteCarType(id uint) (map[string]interface{}) {
	carType := &CarType{}
	err := GetDB().Table("car_types").Where("id = ?", id).First(carType).Error
	if err != nil {
		return nil
	}

	GetDB().Delete(&carType)

	resp := u.Message(true, "success")
	return resp
}