package models

import (
	"github.com/jinzhu/gorm"
	u "go-rentel/utils"
)

type Province struct {
	gorm.Model
	Name string `json:"name"`
}

func (province *Province) Validate() (map[string]interface{}, bool) {

	if province.Name == "" {
		return u.Message(false, "Province name should be on the payload"), false
	}

	//All the required parameters are present
	return u.Message(true, "success"), true
}

func (province *Province) Create() (map[string]interface{}) {

	if resp, ok := province.Validate(); !ok {
		return resp
	}

	GetDB().Create(province)

	resp := u.Message(true, "success")
	resp["province"] = province
	return resp
}

func GetProvince(id uint) (*Province) {

	province := &Province{}
	err := GetDB().Table("provinces").Where("id = ?", id).First(province).Error
	if err != nil {
		return nil
	}
	return province
}

func FetchProvince() ([]*Province) {

	provinces := make([]*Province, 0)
	err := GetDB().Table("provinces").Find(&provinces).Error
	if err != nil {
		return nil
	}

	return provinces
}

func UpdateProvince(id uint, newProvince *Province) (*Province) {

	err := GetDB().Table("provinces").Where("id = ?", id).Update(newProvince).Error
	if err != nil {
		return nil
	}

	province := &Province{}
	err = GetDB().Table("provinces").Where("id = ?", id).First(province).Error
	if err != nil {
		return nil
	}
	return province
}

func DeleteProvince(id uint) (map[string]interface{}) {
	province := &Province{}
	err := GetDB().Table("provinces").Where("id = ?", id).First(province).Error
	if err != nil {
		return nil
	}

	GetDB().Delete(&province)

	resp := u.Message(true, "success")
	return resp
}