package models

import (
	"github.com/jinzhu/gorm"
	u "go-rentel/utils"
)

type Car struct {
	gorm.Model
	Name      string `json:"name"`
	Photo     string `json:"photo"`
	CtPsg     uint   `json:"ctPsg"`
	IsAT      uint   `json:"isAT"`
	CarTypeId uint   `json:"car_type_id"`
	CityId    uint   `json:"city_id"`
	UserId    uint   `json:"user_id"`
}

func (car *Car) Validate() (map[string]interface{}, bool) {

	if car.Name == "" {
		return u.Message(false, "Car name should be on the payload"), false
	}

	if car.CarTypeId <= 0 {
		return u.Message(false, "Car type is not recognized"), false
	}

	if car.CityId <= 0 {
		return u.Message(false, "City is not recognized"), false
	}

	if car.UserId <= 0 {
		return u.Message(false, "User is not recognized"), false
	}

	//All the required parameters are present
	return u.Message(true, "success"), true
}

func (car *Car) Create() (map[string]interface{}) {

	if resp, ok := car.Validate(); !ok {
		return resp
	}

	GetDB().Create(car)

	resp := u.Message(true, "success")
	resp["car"] = car
	return resp
}

func GetCar(id uint) (*Car) {

	car := &Car{}
	err := GetDB().Table("car_s").Where("id = ?", id).First(car).Error
	if err != nil {
		return nil
	}
	return car
}

func FetchCar() ([]*Car) {

	cars := make([]*Car, 0)
	err := GetDB().Table("car_s").Find(&cars).Error
	if err != nil {
		return nil
	}

	return cars
}

func UpdateCar(id uint, userId uint, newCar *Car) (*Car) {

	err := GetDB().Table("car_s").Where("id = ?", id).Where("user_id = ?", userId).Update(newCar).Error
	if err != nil {
		return nil
	}

	car := &Car{}
	err = GetDB().Table("car_s").Where("id = ?", id).Where("user_id = ?", userId).First(car).Error
	if err != nil {
		return nil
	}
	return car
}

func DeleteCar(id uint, userId uint) (map[string]interface{}) {
	
	car := &Car{}
	err := GetDB().Table("car_s").Where("id = ?", id).Where("user_id = ?", userId).First(car).Error
	if err != nil {
		return nil
	}

	GetDB().Delete(&car)

	resp := u.Message(true, "success")
	return resp
}