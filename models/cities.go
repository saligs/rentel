package models

import (
	"github.com/jinzhu/gorm"
	u "go-rentel/utils"
)

type City struct {
	gorm.Model
	Name       string `json:"name"`
	ProvinceId uint   `json:"province_id"`
}

func (city *City) Validate() (map[string]interface{}, bool) {

	if city.Name == "" {
		return u.Message(false, "City name should be on the payload"), false
	}

	//All the required parameters are present
	return u.Message(true, "success"), true
}

func (city *City) Create() (map[string]interface{}) {

	if resp, ok := city.Validate(); !ok {
		return resp
	}

	GetDB().Create(city)

	resp := u.Message(true, "success")
	resp["city"] = city
	return resp
}

func GetCity(id uint) (*City) {

	city := &City{}
	err := GetDB().Table("cities").Where("id = ?", id).First(city).Error
	if err != nil {
		return nil
	}
	return city
}

func FetchCity() ([]*City) {
	
	cities := make([]*City, 0)
	err := GetDB().Table("cities").Find(&cities).Error
	if err != nil {
		return nil
	}

	return cities
}

func FetchCityByProvince(provinceId int) ([]*City) {
	
	cities := make([]*City, 0)
	err := GetDB().Table("cities").Where("province_id = ?", provinceId).Find(&cities).Error
	if err != nil {
		return nil
	}

	return cities
}

func UpdateCity(id uint, newCity *City) (*City) {

	err := GetDB().Table("cities").Where("id = ?", id).Update(newCity).Error
	if err != nil {
		return nil
	}

	city := &City{}
	err = GetDB().Table("cities").Where("id = ?", id).First(city).Error
	if err != nil {
		return nil
	}
	return city
}

func DeleteCity(id uint) (map[string]interface{}) {
	city := &City{}
	err := GetDB().Table("cities").Where("id = ?", id).First(city).Error
	if err != nil {
		return nil
	}

	GetDB().Delete(&city)

	resp := u.Message(true, "success")
	return resp
}